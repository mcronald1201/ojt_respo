<!-- To connect the sections in base -->
<?= $this->extend('templates/base') ?>

<!-- Title section: Open & Close-->
<?= $this->section("head") ?>
<title>CRUD</title>

<?= $this->endSection() ?>

<!-- Body section: Open -->
<?= $this->section("content") ?>

<!--MODAL: Create Users -->
<div class="modal fade" id="mdlcreate" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Create New User</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Card: Open -->
                <div class="card">
                    <!-- Card Body: Open -->
                    <div class='card-body'>
                        <!-- FORM: Creating User -->
                        <form action="javascript:void(0)" id='frmcu' enctype="multipart/form-data">
                            <!-- Row A: Open -->
                            <div class="row row-cols-3 g-3">
                                <!-- ==== 1ST ROW ====-->
                                <div class="col form-group">
                                    <!-- User: FN -->
                                    <label for="txtfn" class="form-label">First Name</label>
                                    <input type="text" class="form-control" id="txtfn" name='first_name' placeholder="First Name" required>

                                </div>
                                <div class="col form-group">
                                    <!-- User: LN -->
                                    <label for="txtln" class="form-label">Middle Name</label>
                                    <input type="text" class="form-control" id="txtmn" name='middle_name' placeholder="Last Name" required>

                                </div>
                                <div class="col form-group">
                                    <!-- User: LN -->
                                    <label for="txtln" class="form-label">Last Name</label>
                                    <input type="text" class="form-control" id="txtln" name='last_name' placeholder="Last Name" required>

                                </div>

                                <!-- ==== 2ND ROW ====-->
                                <div class="col form-group">
                                    <!-- User: Email -->
                                    <label for="txtemail" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="txtemail" name='email' placeholder="Email" required>
                                </div>

                                <div class="col form-group">
                                    <!-- User: Phone # -->
                                    <label for="txtphone" class="form-label">Phone</label>
                                    <input type="text" class="form-control" id="txtphone" name="phone_num" placeholder="Cellphone number" required>
                                </div>

                                <div class='col'>
                                    <div class="row row-cols-2">
                                        <div class="col form-group">
                                            <!-- User: Gender -->
                                            <label for="ddlgender" class="form-label">Gender</label>
                                            <select class="form-select" id='ddlgender' name='gender' required>
                                                <option value="Male" selected>Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>

                                        <div class="col form-group">
                                            <!-- User: Age -->
                                            <label for="txtage" class="form-label">Age</label>
                                            <input type="number" class="form-control" id="txtage" name="age" min="12" max="100" placeholder="Between 12-100" required>
                                        </div>

                                    </div>
                                </div>

                                <!-- ==== 3RD ROW ====-->
                                <div class="col form-group">
                                    <!-- User: Address -->
                                    <label for="txtaddress" class="form-label">Address</label>
                                    <input type="text" class="form-control" id="txtaddress" name="address" placeholder="Current Address" required>
                                </div>

                                <div class="col form-group">
                                    <!-- User: City -->
                                    <label for="txtcity" class="form-label">City</label>
                                    <input type="text" class="form-control" id="txtcity" name="city" placeholder="City" required>
                                </div>

                                <div class="col form-group">
                                    <!-- User: Province -->
                                    <label for="txtprovince" class="form-label">Province</label>
                                    <input type="text" class="form-control" id="txtprovince" name="province" placeholder="Province" required>
                                </div>

                                <!-- ROW : Close -->
                            </div>
                            <!-- Card Body: Close -->
                    </div>
                    <!-- Card: Close -->
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline-success" id='btnsave'>Save</button>
                <!-- Form: Close -->
                </form>
            </div>
        </div>
    </div>
    <!--MODAL: Create Users(Close) -->
</div>


<!--MODAL: View User Profile  -->
<div class="modal fade" id="mdlvup" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <!-- Modal Content: Open -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">User Profile</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!-- Modal Body: Open -->
            <div class="modal-body">
                <!-- Card: Open -->
                <div class="card">
                    <!-- Card Body: Open -->
                    <div class='card-body'>
                        <div class="row row-cols-2 g-3">

                            <div class='col form-group'>
                                <div class="row row-cols-1 g-2">
                                    <div class="col form-group">
                                        <!-- Update User Profile: img  "  -->
                                        <img id='user_profile' class='rounded' style="object-fit:contain;
                                        width:300px;
                                        height:300px;
                                        " />
                                    </div>

                                    <div class="col form-group offset-md-2 mt-2">
                                        <!-- Update User: img  "  -->
                                        <button type="button" class="btn btn-info" data-bs-target="#mdlcp" data-bs-toggle="modal">
                                            Change Profile Picture
                                        </button>
                                    </div>
                                </div>
                            </div>


                            <div class='col form-group'>
                                <h5 class='fw-bold text-info text-uppercase' id='txtfullname'></h5>
                                <div class="row row-cols-2 g-3">
                                    <input type='hidden' id='txtuserid'></input>
                                    <div class='col form-group'>
                                        <!-- Update User Info: Email -->
                                        <b class='fw-bold h6'>Email:</b> &nbsp;<p class='h7' id='txtvpemail'></p>
                                    </div>
                                    <div class='col form-group'>
                                        <!-- Update User Info: Phone # -->
                                        <b class='fw-bold h6'>Phone #:</b> &nbsp;<p class='h7' id='txtvpphone'></p>
                                    </div>
                                    <div class='col form-group'>
                                        <!-- Update User Info: Age -->
                                        <b class='fw-bold h6'>Age:</b> &nbsp;<p class='h7' id='txtvpage'></p>
                                    </div>
                                    <div class='col form-group'>
                                        <!--Update  User Info: Gender -->
                                        <b class='fw-bold h6'>Gender:</b> &nbsp;<p class='h7' id='txtvpgender'></p>
                                    </div>

                                    <div class='col form-group'>
                                        <!-- Update User Info: City -->
                                        <b class='fw-bold h6'>City:</b> &nbsp;<p class='h7' id='txtvpcity'></p>
                                    </div>
                                    <div class='col form-group'>
                                        <!-- Update User Info: Province -->
                                        <b class='fw-bold h6'>Province:</b> &nbsp;<p class='h7' id='txtvpprovince'></p>
                                    </div>
                                </div>
                                <div class='col form-group'>
                                    <!-- Update User Info: Address -->
                                    <b class='fw-bold h6'>Address:</b> &nbsp;<p class='h7' id='txtvpaddress'></p>
                                </div>
                            </div>
                            <!-- Card Body: Close -->
                        </div>
                        <!-- Card: Close -->
                    </div>
                    <!-- Modal Body: Close -->
                </div>
                <!-- Modal Content: Close -->
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-warning" id='btnedit' data-bs-target="#mdleup" data-bs-toggle="modal">Edit</button>
            </div>
        </div>
    </div>
    <!--MODAL: View User Profile(Close) -->
</div>

<!--MODAL: Edit User Profile  -->
<div class="modal fade" id="mdleup" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Edit Profile</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Card: Open -->
                <div class="card">
                    <!-- Card Body: Open -->
                    <div class='card-body'>
                        <!-- FORM: Update User -->
                        <form action="javascript:void(0)" id='frmupdate' enctype="multipart/form-data">
                            <!-- Row : Open -->
                            <div class="row row-cols-3 g-3">

                                <!-- ==== 1ST ROW ====-->
                                <div class="col form-group">
                                    <!-- Update User: FN -->
                                    <label for="txtfn" class="form-label">First Name</label>
                                    <input type="text" class="form-control" id="txtufn" name="ufn" placeholder="First Name" required>

                                </div>

                                <div class="col form-group">
                                    <!-- Update User: MN --> 
                                    <label for="txtmn" class="form-label">Middle Name
                                        <input type="text" class="form-control" id="txtumn" name="umn" placeholder="Middle Name" required>

                                </div>

                                <div class="col form-group">
                                    <!-- Update User: LN -->
                                    <label for="txtln" class="form-label">Last Name</label>
                                    <input type="text" class="form-control" id="txtuln" name="uln" placeholder="Last Name" required>
                                </div>

                                <!-- ==== 2ND ROW ====-->
                                <div class="col form-group">
                                    <!-- Update User: Email --> 
                                    <label for="txtemail" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="txtuemail" name="uemail" placeholder="Email" required>
                                </div>

                                <div class="col form-group">
                                    <!-- Update User: Phone # -->
                                    <label for="txtphone" class="form-label">Phone</label>
                                    <input type="text" class="form-control" id="txtuphone" name="uphone" placeholder="Cellphone number" required>

                                </div>

                                <div class='col'>
                                    <div class="row row-cols-2">
                                        <div class="col form-group">
                                            <!-- Update User: Gender -->
                                            <label for="ddlgender" class="form-label">Gender</label>
                                            <select class="form-select" id='ddlugender' name="ugender" required>
                                                <option value="Male" selected>Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>

                                        <div class="col form-group">
                                            <!-- Update User: Age --> 
                                            <label for="txtage" class="form-label">Age</label>
                                            <input type="number" class="form-control" id="txtuage" name="uage" min="12" max="100" placeholder="Between 12-100" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- ==== 3RD ROW ====-->
                                <div class="col form-group">
                                    <!-- Update User: Address --> 
                                    <label for="txtaddress" class="form-label">Address</label>
                                    <input type="text" class="form-control" id="txtuaddress" name="uaddress" placeholder="Current Address" required>
                                </div>

                                <div class="col form-group">
                                    <!-- Update User: City --> 
                                    <label for="txtcity" class="form-label">City</label>
                                    <input type="text" class="form-control" id="txtucity" name="ucity" placeholder="City" required>
                                </div>

                                <div class="col form-group">
                                    <!-- Update User: Province --> 
                                    <label for="txtprovince" class="form-label">Province</label>
                                    <input type="text" class="form-control" id="txtuprovince" name="uprovince" placeholder="Province" required>
                                </div>

                                <!-- ROW : Close -->
                            </div>
                            <!-- Card Body: Close -->
                    </div>
                    <!-- Card: Close -->
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-secondary" data-bs-target="#mdlvup" data-bs-toggle="modal">Back</button>
                <button type="submit" class="btn btn-outline-success" id='btnsv' disabled>Save Changes</button>
                <!-- Form: Close -->
                </form>

            </div>
        </div>
    </div>
    <!--MODAL: Edit User Profile(Close) -->
</div>

<!-- MODAL: Change Profile (Open) -->
<div class="modal fade" id="mdlcp" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Change Profile Picture</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- Form-->
                <form action="<?= base_url('pages/update_profile_pic') ?>" method='post' enctype="multipart/form-data" id='frmcp'>
                    <input type='hidden' id='txtcpuserid' name='user_id'></input>
                    <div class="col form-group">
                        <!-- Update User Profile: img " -->
                        <label for="fileimg" class="form-label">Profile Picture</label>
                        <input type="file" accept="image/*" class="form-control" id="fileimg" name="img" required>
                    </div>


            </div>
            <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-secondary" data-bs-target="#mdlvup" data-bs-toggle="modal">Back</button>
                <button type="submit" class="btn btn-outline-success" id='btncpsave' disabled>Save Changes</button>
                <!-- Form: close-->
                </form>
            </div>
        </div>
    </div>
    <!-- MODAL: Change Profile (Close) -->
</div>



<div class='container mt-4'>
    <!-- div: Row Open -->
    <div class='row'>
        <!-- div: Col Open -->
        <div class='col-md-8 offset-md-2'>
            <!-- Card: Open -->
            <div class="card">
                <div class='card-header'>
                    <H3> List of Users
                        <!-- Button trigger modal: Create Users -->
                        <button type="button" class="btn btn-info float-end" data-bs-toggle="modal" data-bs-target="#mdlcreate">
                            Create New User
                        </button>
                    </H3>
                </div>
                <!-- Card Body: Open -->
                <div class='card-body'>
                    <!-- Table: Open -->
                    <!-- Table: Users -->
                    <table class='table table-bordered' id='tblusers'>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Date Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class='tblusersbody'></tbody>
                        <!-- Table: Close -->
                    </table>

                    <!-- Card Body: Close -->
                </div>
                <!-- Card: Close -->
            </div>
            <!-- div: Row Close -->
        </div>
        <!-- div: Col Close -->
    </div>
</div>


<!-- Body section: Close -->
<?= $this->endSection() ?>

<!-- Script section: Open -->
<?= $this->section("script") ?>

<script>
    // Message: Change Profile
    <?php
    $msg = session()->getFlashdata('status');
    if (session("success")) { ?>
        alertify.notify('Profile picture changed successfully', 'success', 5);
    <?php } elseif (session("fail")) { ?>
        alertify.notify('Failed to change profile picture', 'error', 5);
    <?php } ?>

    $(document).ready(function() {
        /* =================================
           ========== E V E N T S ==========
           =================================
        */
        // Modal: Create User textboxes
        only_letters($('#txtfn'));
        only_letters($('#txtmn'));
        only_letters($('#txtln'));
        only_letters($('#txtcity'));
        only_letters($('#txtprovince'));

        // Modal: Edit Profile Info textboxes
        only_letters($('#txtufn'));
        only_letters($('#txtumn'));
        only_letters($('#txtuln'));
        only_letters($('#txtucity'));
        only_letters($('#txtuprovince'));
        console.log('MODIFY: 7');

        // On-Change: Modal(Change Profile) 
        $("#fileimg").change(function() {
            /// Disabling "save changes button" ///
            if ($("#fileimg").val() != '') {
                $("#btncpsave").prop('disabled', false);
            } else {
                $("#btncpsave").prop('disabled', true);
            }
        })

        // On-Change: Modal(Change Profile)  
        function disable_btnsv(arr_id){
            /// Enabling btnsv if any of arr_id change ///
            arr_id.forEach(function( index ) {
                index.change(function() {
                /// Enabling "save changes button" ///
                    $("#btnsv").prop('disabled', false);

                })
              
            });
        }

        // Text id in  Modal(Change Profile)  
        var txt_id = [$("#txtufn"), $("#txtumn"), $("#txtuln"), $("#txtuemail"), 
                    $("#txtuphone"), $("#ddlugender"), $("#txtuage"), $("#txtuaddress"),
                    $("#txtucity"), $("#txtuprovince")];

        disable_btnsv(txt_id);  // Show Results
        





        /* =================================
           ========= D E L E T E ===========
           =================================
        */

        $(document).on('click', '#btndelete', function() {
            /// Deleting User ///
            // Getting the closest TR class
            var user_id = $(this).closest('tr').find('.user_id').text();
            alertify.confirm("Are you sure you want to delete?", function(asc) {
                if (asc) {
                    $.ajax({
                        method: 'post',
                        url: 'pages/delete',
                        data: {
                            'user_id': user_id
                        },
                        success: function(response) {
                            loadusers(); // Reload user list   
                            alertify.success("Record is deleted."); //ajax call for delete 
                        }
                    });
                } else {
                    alertify.message('Canceled deleting');
                }
            }, "Default Value");

        });



        /* =================================
           ============ R E A D ============
           =================================
        */
        function loadusers() {
            /// Load Data Table ///
            $.ajax({
                method: 'get', // Method Type
                url: '<?= base_url('/pages/read'); ?>', // Action
                success: function(response) {
                    var data = response.user_info;
                    $('.tblusersbody').html(''); // Remove Previous Data
                    $("#btnsv").prop('disabled', true); // Disable button in Change profile modal

                    // Add new Data
                    for (var i = 0; i < data.length; i++) {
                        $('.tblusersbody').append(`
                            <tr>
                                <td class='d-flex justify-content-center user_id '>${data[i]['user_id']}</td>
                                <td class='pe-0 me-0 '>
                                    <div class="clearfix content-heading">
                                        

                                        <button type="button" class="btn btn-link text-decoration-none text-info fw-bold" id="btnview">
                                        <img src="${data[i]['img']}" class="rounded-circle " style="object-fit:contain;
                                        width:30px;
                                        height:30px;
                                        " />  ${data[i]['first_name']} ${data[i]['middle_name']} ${data[i]['last_name']}
                                        </button>
                                    </div>

                                </td>
                                <td>${data[i]['date_created']}</td>
                                <td>
                                    <button class="badge btn-danger" id="btndelete">Delete</button>
                                </td>
                            </tr>
                        `)
                    }
                }

            })
        }

        loadusers(); // Load function

        $(document).on('click', '#btnview', function() {
            /// View Profile of User ///
            // Getting the closest TR class
            var user_id = $(this).closest('tr').find('.user_id').text();

            $.ajax({
                method: 'post',
                url: '<?= base_url('/pages/view_profile'); ?>',
                data: {
                    'user_id': user_id
                },
                success: function(response) {
                    var data = response.user_info;
                    console.log('USER PROFILE', response.user_info);

                    $("#mdlvup").modal('show'); // show modal: View User Profile 
                    // Fill all user profile info (View Profile Modal) 
                    $('#txtuserid').val(user_id);
                    $("#txtfullname").text(data['first_name'] + " " + data['middle_name'] + " " + data['last_name']);
                    $("#txtvpemail").text(data['email']);
                    $("#txtvpphone").text(data['phone_num']);
                    $("#txtvpage").text(data['age']);
                    $("#txtvpgender").text(data['gender']);
                    $("#txtvpcity").text(data['city']);
                    $("#txtvpprovince").text(data['province']);
                    $("#txtvpaddress").text(data['address']);

                    // Fill all user profile info (Update Modal)


                    $('#txtcpuserid').val(user_id);
                    $('#txtufn').val(data['first_name']);
                    $('#txtumn').val(data['middle_name']);
                    $('#txtuln').val(data['last_name']);
                    $('#txtuemail').val(data['email']);
                    $('#ddlugender').val(data['gender']);
                    $('#txtuage').val(data['age']);
                    $('#txtuphone').val(data['phone_num']);
                    $("#user_profile").attr("src", data['img']);
                    $('#txtuaddress').val(data['address']);
                    $('#txtucity').val(data['city']);
                    $('#txtuprovince').val(data['province']);



                }
            });
        });
        /* =================================
           ========= U P D A T E ===========
           =================================
        */
        // === Form Validation: Update Profile ===
        function validate_form2() {
            $("#frmupdate").validate({
                // Form Validation: Rules (Set the requirements/rules)
                rules: {
                    ufn: {
                        required: true,
                        minlength: 2,
                    },
                    umn: {
                        required: true,
                        minlength: 2
                    },
                    uln: {
                        required: true,
                        minlength: 2
                    },
                    uemail: {
                        required: true,
                        email: true,
                        minlength: 2,
                    },
                    uphone: {
                        required: true,
                        minlength: 2
                    },
                    uage: {
                        required: true,
                        minlength: 2
                    },
                    uimage: {
                        required: true
                    },
                    uaddress: {
                        required: true,
                        minlength: 2
                    },
                    ucity: {
                        required: true,
                        minlength: 2
                    },
                    uprovince: {
                        required: true,
                        minlength: 2
                    },

                },
                // Form Validation: Message (Customize error message)
                messages: {
                    ufn: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters',
                    },
                    umn: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    uln: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    uemail: {
                        required: 'This field is required.',
                        email: 'Please enter a valid email address',
                        minlength: 'at least 2 characters'
                    },
                    uphone: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    uage: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    uimage: {
                        required: 'This field is required.'
                    },
                    uaddress: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    ucity: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    uprovince: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                },

            });
        };

        validate_form2(); // Run Validate form: Update User

        // === Form Validation:  Update User ===
        $('#frmupdate').on('submit', function(e) {
            e.preventDefault();
            if ($('#txtufn').val().length <= 1 || $('#txtumn').val() <= 1 || $('#txtuln').val() <= 1 ||
                $('#txtuemail').val().length <= 1 || $('#ddlugender').val().length <= 1 || $('#txtuage').val().length <= 1 ||
                $('#txtuphone').val().length <= 1 || $('#txtuaddress').val().length <= 1 || $('#txtucity').val().length <= 1 ||
                $('#txtuprovince').val().length <= 1 || $('#txtuage').val() < 12 || $('#txtuage').val() > 100) {
                validate_form2()

            } else {

                $.ajax({
                    method: 'get', // Method Type
                    url: '<?= base_url('/pages/update'); ?>', // Action
                    // Ajax Data
                    data: {
                        'user_id': $('#txtuserid').val(),
                        'first_name': $('#txtufn').val(),
                        'middle_name': $('#txtumn').val(),
                        'last_name': $('#txtuln').val(),
                        'email': $('#txtuemail').val(),
                        'gender': $('#ddlugender').val(),
                        'age': $('#txtuage').val(),
                        'phone_num': $('#txtuphone').val(),
                        'address': $('#txtuaddress').val(),
                        'city': $('#txtucity').val(),
                        'province': $('#txtuprovince').val()
                    },

                    success: function(response) {
                        // Getting the Messages/Response  
                        $("#mdlvup").modal('hide'); // Hide Modal: View User Profile
                        $("#mdlvup").find('input').val(''); // Clear all Fields in modal in View User Profile
                        $("#mdleup").modal('hide'); // Hide Modal: Update Profile
                        $("#mdleup").find('input').val(''); // Clear all Fields in modal in Update Profile

                        loadusers();
                        if (response.success == true) {
                            alertify.notify(response.msg, 'success', 5);
                        } else {
                            alertify.notify(response.msg, 'error', 5);

                        }


                    }

                });
            }

        });





        /* =================================
           ========= C R E A T E ===========
           =================================
        */

        // === Form Validation ===
        function validate_form() {
            $("#frmcu").validate({
                // Form Validation: Rules (Set the requirements/rules)
                rules: {
                    first_name: {
                        required: true,
                        minlength: 2
                    },
                    middle_name: {
                        required: true,
                        minlength: 2
                    },
                    last_name: {
                        required: true,
                        minlength: 2
                    },
                    email: {
                        required: true,
                        email: true,
                        minlength: 2,
                        remote: {
                            url: "<?= base_url('/pages/email_exist') ?>",
                            type: "post",
                            data: {
                                'email': function() {
                                    return $("#txtemail").val();
                                }
                            }
                        }
                    },
                    phone_num: {
                        required: true,
                        minlength: 2
                    },
                    age: {
                        required: true,
                        minlength: 2
                    },
                    address: {
                        required: true,
                        minlength: 2
                    },
                    city: {
                        required: true,
                        minlength: 2
                    },
                    province: {
                        required: true,
                        minlength: 2
                    },

                },
                // Form Validation: Message (Customize error message)
                messages: {
                    first_name: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    middle_name: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    last_name: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    email: {
                        required: 'This field is required.',
                        email: 'Please enter a valid email address',
                        minlength: 'at least 2 characters',
                        remote: 'Email Already used'
                    },
                    phone_num: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    age: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    address: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    city: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                    province: {
                        required: 'This field is required.',
                        minlength: 'at least 2 characters'
                    },
                },

            });

        };


        validate_form() // Run Validate form: Create User

        // === Form Validation:  Create User ===
        $('#frmcu').on('submit', function(e) {
            e.preventDefault();

            if ($('#txtfn').val().length <= 1 || $('#txtmn').val() <= 1 || $('#txtln').val() <= 1 ||
                $('#txtemail').val().length <= 1 || $('#ddlgender').val().length <= 1 || $('#txtage').val().length <= 1 ||
                $('#txtphone').val().length <= 1 || $('#txtaddress').val().length <= 1 || $('#txtcity').val().length <= 1 ||
                $('#txtprovince').val().length <= 1 || $('#txtage').val() < 12 || $('#txtage').val() > 100) {
                validate_form()

            } else {
                $.ajax({
                    method: 'post', // Method Type
                    url: '<?= base_url('/pages/create'); ?>', // Action
                    // Ajax Data
                    data: {
                        'first_name': $('#txtfn').val(),
                        'middle_name': $('#txtmn').val(),
                        'last_name': $('#txtln').val(),
                        'email': $('#txtemail').val(),
                        'gender': $('#ddlgender').val(),
                        'age': $('#txtage').val(),
                        'phone_num': $('#txtphone').val(),
                        'address': $('#txtaddress').val(),
                        'city': $('#txtcity').val(),
                        'province': $('#txtprovince').val()
                    },
                    success: function(response) {
                        // Getting the Messages/Response
                        $("#mdlcreate").modal('hide'); // Hide Modal: Create User
                        $("#mdlcreate").find('input').val(''); // Clear all Fields in modal in Create User
                        loadusers();

                        if (response.success == true) {
                            alertify.notify(response.msg, 'success', 5);
                        } else {
                            alertify.notify(response.msg, 'error', 5);

                        }


                    }

                });
            }

        });





    });
</script>

<!-- Script section: Close -->
<?= $this->endSection() ?>



ou