<?php

namespace App\Controllers;

# Import UsersModel
use App\Models\UsersModel;
use App\Models\CustomeModel;


class Pages extends BaseController
{
    public function index()
    {
        /// Index Page Controller ///

        return view('templates/read');
    }

    public function create()
    {
        /// Create Page Controller ///
        if ($this->request->getMethod() == "post") {
            # Determine the response message and type
            $response = [
                'success' => '',
                'msg' => ''
            ];
            $gender = $this->request->getVar('gender');  // User's Gender
            # Setting Default Profile
            if ($gender == 'Male'){
                $img_file = base_url('assets/images/male_profile.png');
            }else{
                $img_file = base_url('assets/images/female_profile.png');
            }
            
            $model = new UsersModel();  # Use Database model class
            # Get the variables in Modal: Create users
            
            $data = [
                'first_name' => $this->request->getVar('first_name'),
                'middle_name' => $this->request->getVar('middle_name'),
                'last_name' => $this->request->getVar('last_name'),
                'email' => $this->request->getVar('email'),
                'phone_num' => $this->request->getVar('phone_num'),
                'age' => $this->request->getVar('age'),
                'gender' => $gender ,
                'img' => $img_file,
                'address' => $this->request->getVar('address'),
                'city' => $this->request->getVar('city'),
                'province' => $this->request->getVar('province')
            ];

            # Insert Data in database (if true)
            if ($model->save($data)) {
                $response['success'] = true;
                $response['msg'] = 'New user successfully added';
            } else {
                $response['success'] = false;
                $response['msg'] = 'Failed to register new user';
            }

            return $this->response->setJSON($response);
        }
    }

    public function read()
    {
        /// Read Page Controller ///
        $model = New UsersModel();
        $data['user_info'] = $model->findAll();
        return $this->response->setJSON($data);
    }

    public function view_profile(){
        /// View Profile Page Controller ///
        $model = new UsersModel();
        $user_id = $this->request->getPost('user_id');
        $data['user_info'] = $model->find($user_id);
        return $this->response->setJSON($data);
    }

    public function update(){
        /// Update Profile Page Controller ///
        $user_id = $this->request->getVar('user_id');
        $data = [
            'first_name' => $this->request->getVar('first_name'),
            'middle_name' => $this->request->getVar('middle_name'),
            'last_name' => $this->request->getVar('last_name'),
            'email' => $this->request->getVar('email'),
            'phone_num' => $this->request->getVar('phone_num'),
            'age' => $this->request->getVar('age'),
            'gender' => $this->request->getVar('gender'),
            'address' => $this->request->getVar('address'),
            'city' => $this->request->getVar('city'),
            'province' => $this->request->getVar('province')
        ];
        $model = new UsersModel(); 
        # Update Data in database (if true)
        if ($model->update($user_id, $data)) {
            $response['success'] = true;
            $response['msg'] = 'Update successfully';
        } else {
            $response['success'] = false;
            $response['msg'] = 'Failed to update';
        }
        return $this->response->setJSON($response);
        
    }

    public function update_profile_pic(){
        /// Change Profile Picture Page Controller ///
        $user_id = $this->request->getVar('user_id');
        $response = [
            'success' => '',
            'msg' => ''
        ];
        
        $file = $this->request->getFile('img');  # get fileupload: img
        if ($file->isValid() && ! $file->hasMoved()) {
            $img_name = $file->getRandomName();  # Renaming randomly
    
            # Save the file in local machine(if True)
            if ($file->move('assets/images/', $img_name)) {
                $data = ['img' => 'assets/images/' . $img_name];
                $model = new UsersModel();
                if ($model->update($user_id, $data)) {
                    # Change Profile Picture:  Success
                    //$response['success'] = true;
                    //$response['msg'] = 'Profile picture changed successfully';
                    return redirect()->to('/')->with('success', 'success');
                } else {
                    # Change Profile Picture:  Failed
                    //$response['success'] = false;
                    //$response['msg'] = 'Failed to change profile picture';
                    return redirect()->to('/')->with('fail', 'fail');
                }
                //return $this->response->setJSON($response);
            
            # Cannot be save img file in local machine
            } //else {
              //  $response['success'] = false;
              //  $response['msg'] = 'Failed to upload image file';
              //  }

            return redirect()->to('/')->with('status', 'failer');
        }     
    }

    public function delete(){
        /// Delete User Page Controller ///
        $model = new UsersModel();
        $model->delete($this->request->getPost('user_id'));
        $response['success'] = true;
        $response['msg'] = 'Successfully Deleted';
        return $this->response->setJSON($response);
    }

    public function email_exist()
    {
        /// Check if the Email exist ///
        $db = db_connect();
        $model = new CustomeModel($db);
        $email = $this->request->getVar('email');
        $getData = $model->check_email($email);
        if (count($getData) > 0) {
            return json_encode(FALSE);
        } else {
            return json_encode(TRUE);;
        }
    }
}
