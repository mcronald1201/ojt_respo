<?php namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model{
    /// Database Model: dbreward

    protected $table = 'users';  # Table name
    protected $primaryKey = 'user_id'; # Primary Key in Table

    # Mutable Fields
    protected $allowedFields = ['first_name', 'middle_name', 'last_name',
                                'email', 'phone_num', 'age', 'gender', 'img', 'address',
                                'city', 'province'];
    


}