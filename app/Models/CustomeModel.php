<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

class CustomeModel{
    protected $db;

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function check_email($email){
        /// Checking if email exist ///
        
        # Query
        $builder = $this->db->table('users') # Table name
                            ->where('email', $email);
                            
	    $result = $builder->get()->getResult();
        return $result;
	    
    }


}